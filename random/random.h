#ifndef ZZGO_RANDOM_RANDOM_H
#define ZZGO_RANDOM_RANDOM_H

#include "engine.h"

struct engine *engine_random_init(char *arg, struct board *b);

#endif
